import React, {StyleSheet, Dimensions, PixelRatio} from "react-native";
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

export default StyleSheet.create({
    "h1": {
        "marginTop": "2rem!important"
    },
    "h1 span": {
        "fontSize": 1.8
    },
    "mt": {
        "marginTop": 3
    },
    "tmp": {
        "fontSize": 4
    }
});