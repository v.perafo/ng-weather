import { Component, NgZone } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { CompleterService, CompleterData } from 'ng2-completer';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import * as weather from 'openweather-apis';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  searchForm: FormGroup;
  title = 'OpenWeather';
  dataList:any;
  data:any;
  url:string = 'https://raw.githubusercontent.com/David-Haim/CountriesToCitiesJSON/master/countriesToCities.json';
  urlIcons:string = 'http://openweathermap.org/img/w/';
  srcIcon:string;
  cityName:any;

  queryList:any;

  searchTerm: string = '';
  searchControl: FormControl;
  searching:boolean = false;
  qCities:any[];

  constructor(private http: Http, private formBuilder: FormBuilder, private zone: NgZone){

    this.searchForm = this.formBuilder.group({
      city: ['', Validators.compose([Validators.required])]
    });

    this.searchControl = new FormControl();
    this.searchControl.valueChanges.debounceTime(400).subscribe(search => {
      this.searching = false;
    });

    this.searchForm.valueChanges.subscribe(data => {
    //  console.log('Form changes ', data);
     this.searchCity(data);
   });

    weather.setLang('es');
    weather.setUnits('metric');
    weather.setAPPID('fbb79629e13310e85d56bcab8a9cbd74');
    this.getCityList();

  }

  getCityList(){
    this.http.get(this.url).map(res => res.json()).subscribe(cities =>{
      this.dataList = cities['Colombia'];
      // console.log(this.dataList);
    });
  }

  getCityInfo(q){
    this.dataList.filter(city =>{
      return q.title.toLowerCase().indexOf(q.toLowerCase()) > -1;
    });
  }

  setCity(){
    let cityName = this.searchForm.value;
    // console.log('set City to  ' + JSON.stringify(cityName.city));
    if(cityName){
        console.log('set City to  ' + cityName.city);
        weather.setCity(cityName.city);
        this.getWeather();
    }

  }

  getWeather(){
    weather.getAllWeather((err, cli) =>{
      this.zone.run(() => {
        this.data = cli;
        console.log(JSON.stringify(this.data));
        this.srcIcon = this.urlIcons + this.data.weather[0].icon + '.png';
        console.log(this.srcIcon);
      });
	  });
  }

  searchCity(data){
   this.qCities = [];
   let val = data.city;
   console.log('city ' + val);
   this.searching = false;
    if (val && val.trim() != '') {
       this.qCities = this.dataList.filter((item) => {
         this.searching = true;
         console.log(item);
         return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
     });
    }
     console.log('find :D  ' + JSON.stringify(this.qCities));
    //  console.log('find :D  ');

 }
}
