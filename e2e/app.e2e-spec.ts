import { WheatherPage } from './app.po';

describe('wheather App', function() {
  let page: WheatherPage;

  beforeEach(() => {
    page = new WheatherPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
